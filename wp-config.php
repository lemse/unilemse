<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lemse' );

/** MySQL database username */
define( 'DB_USER', 'lemse' );

/** MySQL database password */
define( 'DB_PASSWORD', 'cp01lemse' );

/** MySQL hostname */
define( 'DB_HOST', 'mysql.lemse.com.br' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** Memory Limit */
define( 'WP_MEMORY_LIMIT', '128M' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ZEp.ZEvhn_4&:O_P|{0[)q@9]Nn]mt^cL=6blg<mlAVQs~C#o;cIG@z$!ZHrY)_n' );
define( 'SECURE_AUTH_KEY',  '2crA#9jM>BEML4NkN@h6I`o^M^dJj=}-E!58|~8{GWo T@tOg;$|P?wm5T#NNJ8E' );
define( 'LOGGED_IN_KEY',    'JN/g,K6z+lI%-.qVBPk,$,WtEwE;^Mm@Nyn7+n4rRR>#rIdy~@a@V~7|]3eCXl7:' );
define( 'NONCE_KEY',        '|<|N/Y?n*hE6Ho}L%?cYv)e%So2@eoa sCWAXOj_EXCJC9b|QJOFg=EDu,qwa1uY' );
define( 'AUTH_SALT',        'EIucYBy%y~#]iSJZsi`4G/mtIT6J#$, -dQR>LD}^|t|XR0:NT_z,Pa+<1Ap^(qP' );
define( 'SECURE_AUTH_SALT', '|2^qW9G.y?]E~ab[0< yw-|P#^Y5}Dl}q0$iZ%M I>a^L >6eT.h7f(eZ1R1!M|r' );
define( 'LOGGED_IN_SALT',   'H!UVcjsf:@LS%9}euFrxXkznrd}JKw.I.mdBtl{I}>HP$`ZmF?7_NrD!}R42a{Q<' );
define( 'NONCE_SALT',       'E0C!!V6`<[ipE&tD*HU{l0AZ_^|c9]j6>bR!N<=hF!_M*onnRcGO^tN&ZPUpOsCT' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
