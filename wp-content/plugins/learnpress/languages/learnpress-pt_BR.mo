��    R      �  m   <      �     �          
          0     >  
   S     ^     f     v     }     �     �     �     �     �  
   �     �     �          
  	        "  3   )     ]     n     �     �  
   �     �     �     �     �  
   �  
   	  
   	     	     %	     ;	     D	     M	     ]	     l	     �	     �	     �	     �	     �	     �	     �	     
     
     
     1
     8
     ?
     P
     _
     n
     
  	   �
     �
     �
  :   �
  �   �
  
   �     �     �     �     �     �     �  
   �     �  
                  *     7     D     [  Y  t     �  	   �     �          "     5     G     X     f     y     �     �     �     �     �  !   �  
   �     �                 
   *     5  D   =     �     �     �  !   �  	   �     �     �          	     %     1     =     Q  +   ]     �     �     �     �     �     �     �     �               .     L     T     e     n     ~  	   �     �     �     �     �     �     �     �            �   (     �  	   �     �     �  
          
     
         +  
   C     N     U     \     d     k     r                         D   ,   	          E      .   1   $   P       -       N   6      @   ;              8   L       =          A   K           %      H          '            &                                    J   ?   #           B         O   I          )       Q   4   9   :   (              7   0                              *   G   "   +   F      
   5   M   >   <   C   R   !   3       2   /       %d review %d reviews Add Add New Lesson Additional Information Back to class Become an Instructor Buy %s now Buy Now Buy this course Cancel Course Course Curriculum Courses Courses Page Courses limit Courses tagged &ldquo;%s&rdquo; Curriculum Curriculum is empty Edit Lesson Email Email options Email: %s Emails Error %d: Unable to create order. Please try again. Featured Courses Free Courses / Priced Courses From I am a returning customer. Instructor Instructors Instructors registration Lessons Login successfully. My Courses My courses No courses No courses! Note to administrator Overview Password Pay with Paypal Payment Method Please enter your email. Popular Courses Post Type General NameCourses Preview Lessons Purchased Courses Recent Courses Register successfully. Remove Remove from course Removed Returning customer Review Search Search Course %s Search Courses Search Lessons Search course... Search item Search... Searching course... Start Text of Buy this course with external linkBuy this course The password should be at least twelve characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ) User Email User roleInstructor Username Username or email View Courses View Lesson View courses Your email Your email address Your order courses default-slugcourses mediaRemove slugcourses static-pageLP Courses static-page-nameCourses Project-Id-Version: Package Name
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-04-11 09:01+0700
PO-Revision-Date: 2018-05-01 00:03-0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.0.7
Last-Translator: 
Language: pt_BR
 %d avaliação %d avaliações Adicionar Adicionar Nova Aula Informação Adicional Voltar para Cursos Seja um Professor Comprar %s agora Comprar agora Comprar este curso Cancelar Curso Currículo do Curso Cursos Página de Cursos Limite de cursos Cursos taggeados &ldquo;%s&rdquo; Currículo Currículo vazio Editar Aula E-mail Opções do e-mail E-mail: %s E-mails Erro %d: Não foi possível registrar o seu pedido. Tente novamente. Cursos Destaque Cursos Grátis / Cursos Pagos De Eu já sou um cliente cadastrado. Professor Professores Registro de Professores Aulas Login efetuado com sucesso. Meus Cursos Meus Cursos Não existem cursos Sem cursos! Use para inserir uma informação adicional Visão geral Senha Pagar com PayPal Método de Pagamento Por favor insira seu e-mail. Cursos mais populares Cursos Aulas Anteriores Cursos comprados Cursos Recentes Registo efetuado com sucesso. Remover Remover do curso Removido Já sou cliente Avaliação Pesquisar Pesquisar Curso %s Pesquisar Cursos Pesquisar Aulas Pesquisar curso… Pesquisar item Pesquisar… Pesquisando cursos… Início Comprar este curso Seu senha deve ter até 20 caracteres. Para uma senha segura, use letras maiúsculas e minúsculas, números e símbolos como ! ” ? $ % ^ & ) E-mail do usuário Professor Nome de usuário Nome de usuário ou e-mail Ver Cursos Ver Aula Ver cursos Seu e-mail Seu endereço de e-mail Seu pedido cursos cursos Remover cursos Cursos Cursos 