<?php
/**
 * Course review widget class.
 *
 * @author   ThimPress
 * @package  LearnPress/Course-Review/Classes
 * @version  3.0.1
 */

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Creating the widget
class LearnPress_Course_Review_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'lpr_course_review',
			__( 'Course Review', 'learnpress-course-review' ),
			array( 'description' => __( 'Display ratings and reviews of course', 'learnpress-course-review' ), )
		);
	}


	public function widget( $args, $instance ) {
		wp_enqueue_script( 'course-review', LP_ADDON_COURSE_REVIEW_URL . '/assets/js/course-review.js', array( 'jquery' ), '', true );
		wp_enqueue_style( 'course-review', LP_ADDON_COURSE_REVIEW_URL . '/assets/css/course-review.css' );
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$course_id      = isset( $instance['course_id'] ) ? $instance['course_id'] : 0;
		$show_rate      = isset( $instance['show_rate'] ) ? $instance['show_rate'] : 'no';
		$show_review    = isset( $instance['show_review'] ) ? $instance['show_review'] : 'no';
		$display_amount = isset( $instance['display_amount'] ) ? $instance['display_amount'] : 5;

		if ( ! $course_id ) {
			$course_id = get_the_ID();
		}

		ob_start();
		if ( $show_rate == 'yes' ) {
			$course_rate = learn_press_get_course_rate( $course_id, false );
			$rate_args   = array(
				'course_id'   => $course_id
			,
				'course_rate' => $course_rate
			);
			learn_press_course_review_template( 'shortcode-course-rate.php', $rate_args );
		}

		if ( $show_review == 'yes' ) {
			$course_review = learn_press_get_course_review( $course_id, 1, $display_amount );
			learn_press_course_review_template( 'shortcode-course-review.php', array(
				'course_id'     => $course_id,
				'course_review' => $course_review
			) );
		}

		$content = ob_get_contents();
		ob_clean();
		echo $content;
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$arg            = array(
			'post_type'      => 'lp_course',
			'posts_per_page' => - 1,
			'post_status'    => 'publish'
		);
		$the_query      = new WP_Query( $arg );
		$title          = isset( $instance['title'] ) ? $instance['title'] : __( 'New title', 'wpb_widget_domain' );
		$course_id      = isset( $instance['course_id'] ) ? $instance['course_id'] : '';
		$show_rate      = isset( $instance['show_rate'] ) ? $instance['show_rate'] : 'no';
		$show_review    = isset( $instance['show_review'] ) ? $instance['show_review'] : 'no';
		$display_amount = isset( $instance['display_amount'] ) ? $instance['display_amount'] : 5;
		$option         = '';
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$checked = '';
				if ( $the_query->post->ID == $course_id ) {
					$checked = "selected='selected'";
				}
				$option .= "<option value='{$the_query->post->ID}' {$checked}>{$the_query->post->post_title}</option>";
			}
		}
		// Reset Post Data
		wp_reset_postdata();
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'learnpress-course-review' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'course_id' ); ?>"><?php _e( 'Course Id:', 'learnpress-course-review' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'course_id' ); ?>">
				<?php echo $option; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'show_rate' ); ?>"><?php _e( 'Show Rate:', 'learnpress-course-review' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'show_rate' ); ?>"
                   name="<?php echo $this->get_field_name( 'show_rate' ); ?>" type="checkbox"
                   value="yes" <?php checked( $show_rate, 'yes', true ); ?>/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'show_review' ); ?>"><?php _e( 'Show Review:', 'learnpress-course-review' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'show_review' ); ?>"
                   name="<?php echo $this->get_field_name( 'show_review' ); ?>" type="checkbox"
                   value="yes" <?php checked( $show_review, 'yes', true ); ?> />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'display_amount' ); ?>"><?php _e( 'Amount Display:', 'learnpress-course-review' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'display_amount' ); ?>"
                   name="<?php echo $this->get_field_name( 'display_amount' ); ?>" type="numeric"
                   value="<?php echo esc_attr( $display_amount ); ?>"/>
        </p>
		<?php
	}

// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance                   = $old_instance;
		$instance['title']          = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['course_id']      = ( ! empty( $new_instance['course_id'] ) ) ? strip_tags( $new_instance['course_id'] ) : '';
		$instance['show_rate']      = ( ! empty( $new_instance['show_rate'] ) ) ? strip_tags( $new_instance['show_rate'] ) : '';
		$instance['show_review']    = ( ! empty( $new_instance['show_review'] ) ) ? strip_tags( $new_instance['show_review'] ) : '';
		$instance['display_amount'] = ( ! empty( $new_instance['display_amount'] ) ) ? strip_tags( $new_instance['display_amount'] ) : 5;

		return $instance;
	}

}